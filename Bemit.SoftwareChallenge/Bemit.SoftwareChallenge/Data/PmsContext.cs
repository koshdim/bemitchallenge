﻿using Bemit.SoftwareChallenge.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bemit.SoftwareChallenge.Data
{
    public class PmsContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }

        public DbSet<Models.Task> Tasks { get; set; }

        public PmsContext(DbContextOptions options) : base(options)
        {
        }

        public async Task<IList<Models.Task>> GetAllInProgressTasks()
        {
            var result = await Tasks.FromSql("GetAllInProgressTasks").ToListAsync();
            return result;
        }

        public async Task<IList<Project>> GetAllInProgressProjects()
        {
            var result = await Projects.FromSql("GetAllInProgressProjects").ToListAsync();
            return result;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder
                .Entity<Project>()
                .Property(e => e.State)
                .HasConversion(
                    s => s.ToString(),
                    s => (ProjectState)Enum.Parse(typeof(ProjectState), s));

            modelBuilder
                .Entity<Models.Task>()
                .Property(e => e.State)
                .HasConversion(
                    s => s.ToString(),
                    s => (TaskState)Enum.Parse(typeof(TaskState), s));
        }
    }
}
