﻿using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Bemit.SoftwareChallenge.Data;
using Bemit.SoftwareChallenge.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace Bemit.SoftwareChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        private readonly PmsContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;

        public ReportController(PmsContext context, IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> GetReport()
        {
            var tasks = await _context.GetAllInProgressTasks();
            var projects = await _context.GetAllInProgressProjects();
            var report = new Report(projects, tasks);

            var reportFile = ExcelReportGenerator.Generate(report, _hostingEnvironment.WebRootPath);
            var result = PhysicalFile(reportFile, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

            SetAttachmentHeader(reportFile);

            return result;
        }

        private void SetAttachmentHeader(string filePath)
        {
            var attachmentHeader = new ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(filePath)
            };

            Response.Headers["Content-Disposition"] = attachmentHeader.ToString();
        }
    }
}