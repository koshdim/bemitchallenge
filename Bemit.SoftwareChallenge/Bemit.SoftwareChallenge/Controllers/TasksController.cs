﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Bemit.SoftwareChallenge.Data;
using Bemit.SoftwareChallenge.Models;
using System;

namespace Bemit.SoftwareChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly PmsContext _context;

        public TasksController(PmsContext context)
        {
            _context = context;
        }

        // GET: api/Tasks
        [HttpGet]
        public IEnumerable<Models.Task> GetTasks()
        {
            return _context.Tasks;
        }

        // GET: api/Tasks/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTask([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var task = await _context.Tasks.FindAsync(id);

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        // PUT: api/Tasks/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTask([FromRoute] int id, [FromBody] Models.Task task)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != task.ID)
            {
                return BadRequest();
            }

            var existingTask = await _context.Tasks.FindAsync(id);

            _context.Entry(task).State = EntityState.Modified;

            if (existingTask.State != task.State)
                UpdateParentState(task);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tasks
        [HttpPost]
        public async Task<IActionResult> PostTask([FromBody] Models.Task task)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Tasks.Add(task);

            UpdateParentState(task);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTask", new { id = task.ID }, task);
        }

        // DELETE: api/Tasks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var task = await _context.Tasks.FindAsync(id);
            if (task == null)
            {
                return NotFound();
            }

            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();

            return Ok(task);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchTaskState([FromRoute] int id, [FromQuery] string taskState)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var existingTask = await _context.Tasks.FindAsync(id);
            existingTask.State = (TaskState)Enum.Parse(typeof(TaskState), taskState);

            _context.Entry(existingTask).State = EntityState.Modified;

            UpdateParentState(existingTask);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Updates states of projects starting from current task to the topmost
        /// </summary>
        /// <param name="changeTask">the tasks that have been changed</param>
        private async void UpdateParentState(Models.Task changeTask) // TODO: should be rewritten in db as stored procedure
        {
            var projects = await _context.Projects.ToListAsync();
            var tasks = await _context.Tasks.ToListAsync();
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(changeTask, projects, tasks);

            UpdateChangedProjects(changedProjects);
        }

        private async void UpdateChangedProjects(IEnumerable<Project> projects)
        {
            foreach (var p in projects)
            {
                var existingProject = await _context.Projects.FindAsync(p.ID);
                existingProject.State = p.State;

                _context.Entry(existingProject).State = EntityState.Modified;
            }
        }

        private bool TaskExists(int id)
        {
            return _context.Tasks.Any(e => e.ID == id);
        }
    }
}