﻿using Bemit.SoftwareChallenge.Models;
using System.Collections.Generic;
using System.Linq;

namespace Bemit.SoftwareChallenge.Controllers
{
    public class ProjectStateUpdater
    {
        /// <summary>
        /// Traverses project tree starting from task's project to the top and 
        /// collects projects that must be updated
        /// </summary>
        /// <param name="changedTask"></param>
        /// <param name="projects">All projects in the system</param>
        /// <param name="tasks">All tasks in the system</param>
        /// <returns>Collection of projects that must be updated</returns>
        public static IEnumerable<Project> GetProjectsToBeChanged(Task changedTask,
            IEnumerable<Project> projects,
            IEnumerable<Task> tasks)
        {
            var changedProjects = new List<Project>();
            var parentProject = projects.Single(p => p.ID == changedTask.ProjectID);

            while (true)
            {
                if (CheckProjectNeedsUpdate(parentProject, projects, tasks))
                    changedProjects.Add(parentProject);

                if (parentProject.ParentID == null)
                    break;
                parentProject = projects.Single(p => p.ID == parentProject.ParentID);
            }

            return changedProjects;
        }

        /// <summary>
        /// Checks if passed project's state must be updated. The rules are:
        /// 1. Completed, if all tasks in the project and its subprojects have state Completed
        /// 2. inProgress, if there is at least one task with state inProgress in project or its subprojects
        /// 3. Planned in all other cases
        /// </summary>
        /// <param name="project">Item to check</param>
        /// <param name="projects">All projects in the system</param>
        /// <param name="tasks">All tasks in the system</param>
        /// <returns>Collection of projects that must be updated</returns>
        private static bool CheckProjectNeedsUpdate(Project project, IEnumerable<Project> projects, IEnumerable<Task> tasks)
        {
            var projectTasks = tasks.Where(t => t.ProjectID == project.ID);
            var projectProjects = projects.Where(p => p.ParentID == project.ID);

            if (projectTasks.All(t => t.State == TaskState.Completed) && projectProjects.All(p => p.State == ProjectState.Completed))
                return CheckProjectNeedsUpdate(project, ProjectState.Completed);

            if (projectTasks.Any(t => t.State == TaskState.InProgress) || projectProjects.Any(p => p.State == ProjectState.InProgress))
                return CheckProjectNeedsUpdate(project, ProjectState.InProgress);
            
            return CheckProjectNeedsUpdate(project, ProjectState.Planned);
        }

        private static bool CheckProjectNeedsUpdate(Project project, ProjectState state)
        {
            var result = project.State != state;

            if (result)
                project.State = state;

            return result;
        }
    }
}
