﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bemit.SoftwareChallenge.Models
{
    [Table("Task")]
    public class Task
    {
        [Key]
        [Column("TaskID")]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public TaskState State { get; set; }
        [Column("ParentTaskID")]
        public int? ParentID { get; set; }
        public int ProjectID { get; set; }
    }
}
