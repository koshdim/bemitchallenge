﻿namespace Bemit.SoftwareChallenge.Models
{
    public enum ProjectState
    {
        Planned,
        InProgress,
        Completed
    }
}
