﻿namespace Bemit.SoftwareChallenge.Models
{
    public enum TaskState
    {
        Planned,
        InProgress,
        Completed
    }
}
