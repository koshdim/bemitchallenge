﻿using OfficeOpenXml;
using System.Collections.Generic;
using System.IO;

namespace Bemit.SoftwareChallenge.Models
{
    public class ExcelReportGenerator
    {
        private const string FileName = "report.xlsx";

        public static string Generate(Report report, string webRootFolder)
        {
            var filePath = Path.Combine(webRootFolder, FileName);
            var file = PrepareFile(filePath);

            using (ExcelPackage package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add(nameof(Project));
                GenerateWorksheetForCollection(report.Projects, worksheet);

                worksheet = package.Workbook.Worksheets.Add(nameof(Task));
                GenerateWorksheetForCollection(report.Tasks, worksheet);

                package.Save();
            }
            
            return filePath;
        }

        private static void GenerateWorksheetForCollection<T>(IEnumerable<T> collection, ExcelWorksheet worksheet)
        {
            var rowIndex = 1;
            var columnIndex = 1;
            var properties = typeof(T).GetProperties();
            //Write column headers
            foreach (var property in properties)
            {
                worksheet.Cells[rowIndex, columnIndex].Value = property.Name;
                columnIndex++;
            }

            rowIndex++;
            //Write values
            foreach (var item in collection)
            {
                columnIndex = 1;
                foreach (var property in properties)
                {
                    var value = property.GetValue(item);
                    worksheet.Cells[rowIndex, columnIndex].Value = value;
                    columnIndex++;
                }
                rowIndex++;
            }
        }

        private static FileInfo PrepareFile(string filePath)
        {
            var file = new FileInfo(filePath);

            if (file.Exists)
                file.Delete();
            else
                return file;

            return new FileInfo(filePath);
        }
    }
}
