﻿using System.Collections.Generic;

namespace Bemit.SoftwareChallenge.Models
{
    public class Report
    {
        public IEnumerable<Project> Projects { get; }
        public IEnumerable<Task> Tasks { get; }

        public Report(IList<Project> projects, IList<Task> tasks)
        {
            Projects = projects;
            Tasks = tasks;
        }
    }
}
