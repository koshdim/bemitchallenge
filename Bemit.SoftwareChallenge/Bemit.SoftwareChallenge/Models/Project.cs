﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bemit.SoftwareChallenge.Models
{
    [Table("Project")]
    public class Project
    {
        [Key]
        [Column("ProjectID")]
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
        public ProjectState State { get; set; }
        [Column("ParentProjectID")]
        public int? ParentID { get; set; }
    }
}
