using Bemit.SoftwareChallenge.Controllers;
using Bemit.SoftwareChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Bemit.SoftwareChallenge.Tests
{
    public class ProjectStateUpdaterSingleTests
    {
        private readonly Task task;
        private readonly Project parentProject;
        private readonly IList<Project> projects;
        private readonly IList<Task> tasks;

        public ProjectStateUpdaterSingleTests()
        {
            parentProject = new Project { ID = 123 };
            task = new Task { ID = 42, ProjectID = parentProject.ID };
            projects = new List<Project> { parentProject };
            tasks = new List<Task> { task };
        }

        [Fact]
        public void OrphanTask_Must_ThrowException()
        {
            //Assert
            task.ProjectID = 111;

            //Act
            var ex = Assert.Throws<InvalidOperationException>(() => ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks));

            //Assert
            Assert.NotNull(ex);
        }

        [Fact]
        public void SameState_MustReturn_EmptyCollection()
        {
            //Assert
            task.State = TaskState.Completed;
            parentProject.State = ProjectState.Completed;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Empty(changedProjects);
        }

        [Fact]
        public void SingleCompletedTask_MustReturn_CompletedProject()
        {
            //Assert
            task.State = TaskState.Completed;
            parentProject.State = ProjectState.Planned;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Single(changedProjects);
            Assert.Equal(ProjectState.Completed, changedProjects.Single().State);
        }

        [Fact]
        public void SingleInProgressTask_MustReturn_InProgressProject()
        {
            //Assert
            task.State = TaskState.InProgress;
            parentProject.State = ProjectState.Planned;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Single(changedProjects);
            Assert.Equal(ProjectState.InProgress, changedProjects.Single().State);
        }

        [Fact]
        public void SinglePlannedTask_MustReturn_PlannedProject()
        {
            //Assert
            task.State = TaskState.Planned;
            parentProject.State = ProjectState.InProgress;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Single(changedProjects);
            Assert.Equal(ProjectState.Planned, changedProjects.Single().State);
        }
    }
}
