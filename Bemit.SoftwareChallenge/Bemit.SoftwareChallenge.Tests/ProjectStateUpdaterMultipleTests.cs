﻿using Bemit.SoftwareChallenge.Controllers;
using Bemit.SoftwareChallenge.Models;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Bemit.SoftwareChallenge.Tests
{
    public class ProjectStateUpdaterMultipleTests
    {
        private readonly Task task;
        private readonly Task siblingTask;
        private readonly Task cousinTask;
        private readonly Project project;
        private readonly Project parentProject;
        private readonly Project childProject;

        private readonly IList<Project> projects;
        private readonly IList<Task> tasks;

        public ProjectStateUpdaterMultipleTests()
        {
            parentProject = new Project { ID = 12 };
            project = new Project { ID = 123, ParentID = parentProject.ID };
            childProject = new Project { ID = 1234, ParentID = project.ID };
            task = new Task { ID = 42, ProjectID = project.ID };
            siblingTask = new Task { ID = 421, ProjectID = project.ID };
            cousinTask = new Task { ID = 52, ProjectID = parentProject.ID };

            projects = new List<Project> { parentProject, project, childProject };
            tasks = new List<Task> { task, siblingTask, cousinTask };
        }

        [Fact]
        public void InProgress_Resets_CurrentAndParentProject()
        {
            //Assert
            task.State = TaskState.InProgress;
            project.State = ProjectState.Completed;
            parentProject.State = ProjectState.Completed;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Equal(2, changedProjects.Count());
            Assert.True(changedProjects.All(p => p.State == ProjectState.InProgress));
        }

        [Fact]
        public void Completed_Completes_CurrentAndNotParentProject()
        {
            //Assert
            task.State = TaskState.Completed;
            siblingTask.State = TaskState.Completed;
            cousinTask.State = TaskState.InProgress;

            project.State = ProjectState.InProgress;
            parentProject.State = ProjectState.InProgress;
            childProject.State = ProjectState.Completed;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Single(changedProjects);
            Assert.Equal(ProjectState.Completed, project.State);
            Assert.Equal(ProjectState.InProgress, parentProject.State);
        }

        [Fact]
        public void AllCompleted_Completes_CurrentAndParentProject()
        {
            //Assert
            task.State = TaskState.Completed;
            siblingTask.State = TaskState.Completed;
            cousinTask.State = TaskState.Completed;

            project.State = ProjectState.InProgress;
            parentProject.State = ProjectState.InProgress;
            childProject.State = ProjectState.Completed;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Equal(2, changedProjects.Count());
            Assert.Equal(ProjectState.Completed, project.State);
            Assert.Equal(ProjectState.Completed, parentProject.State);
        }

        [Fact]
        public void AllPlanned_MakePlanned_CurrentAndParentProject()
        {
            //Assert
            task.State = TaskState.Planned;
            siblingTask.State = TaskState.Planned;
            cousinTask.State = TaskState.Planned;

            project.State = ProjectState.Completed;
            parentProject.State = ProjectState.InProgress;
            childProject.State = ProjectState.Completed;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Equal(2, changedProjects.Count());
            Assert.Equal(ProjectState.Planned, project.State);
            Assert.Equal(ProjectState.Planned, parentProject.State);
        }

        [Fact]
        public void ComepletedAndInProgressChildProject_DoesNot_MakeCompleted()
        {
            //Assert
            task.State = TaskState.Completed;
            siblingTask.State = TaskState.Completed;
            cousinTask.State = TaskState.Completed;

            project.State = ProjectState.InProgress;
            parentProject.State = ProjectState.InProgress;
            childProject.State = ProjectState.InProgress;

            //Act
            var changedProjects = ProjectStateUpdater.GetProjectsToBeChanged(task, projects, tasks);

            //Assert
            Assert.Empty(changedProjects);
            Assert.Equal(ProjectState.InProgress, project.State);
            Assert.Equal(ProjectState.InProgress, parentProject.State);
        }
    }
}
