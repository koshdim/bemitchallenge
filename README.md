# bemitchallenge

This is ASP .Net Core API application.
It consists of three controllers:
1. ProjectsController: 
	standard set of CRUD operations, implemeted as Get, Put, Post and Delete methods
2. TasksController:
	a) similar CRUD methods as ProjectsController
	b) Patch method to update state of specific tasks
also TasksController contains logic that updates project's state when task's state changes
3. ReportController:
	single method that returns list of tasks and projects in state "InProgress"
EPPlus.Core package is used for Excel report file generation